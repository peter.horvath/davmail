#!/bin/bash
git remote add origin git@zeta.intra.isento.net:maxx/davmail.git
git remote add upstream https://github.com/mguessan/davmail.git
git remote add gitlab.com git@gitlab.com:peter.horvath/davmail.git
git remote add jbhensley https://github.com/jbhensley/davmail.git
git remote add Levenson https://github.com/Levenson/davmail.git
git remote add vdmz https://github.com/vdmz/davmail.git
